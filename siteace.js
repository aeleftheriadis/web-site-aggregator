Websites = new Mongo.Collection("websites");

Websites.allow({

	// we need to be able to update images for ratings.
	update:function(userId, doc){
		console.log("testing security on image update");
		if (Meteor.user()){// they are logged in
			return true;
		} else {// user not logged in - do not let them update  (rate) the image.
			return false;
		}
	},

	insert:function(userId, doc){
		console.log("testing security on image insert");
		if (Meteor.user()){// they are logged in
			if (userId != doc.createdBy){// the user is messing about
				return false;
			}
			else {// the user is logged in, the image has the correct user id
				return true;
			}
		}
		else {// user not logged in
			return false;
		}
	},
	remove:function(userId, doc){
		if (Meteor.user()){// they are logged in
			if (userId != doc.createdBy){// the user is messing about
				return false;
			}
			else {// the user is logged in, the image has the correct user id
				return true;
			}
		} else {// user not logged in - do not let them update  (rate) the image.
			return false;
		}
	}
})

if (Meteor.isClient) {

	Meteor.subscribe("websites");

	///Account Config
	Accounts.ui.config({
	  requestPermissions: {
	    facebook: ['user_likes'],
	    github: ['user', 'repo']
	  },
	  requestOfflineToken: {
	    google: true
	  },
	  passwordSignupFields: 'USERNAME_AND_OPTIONAL_EMAIL'
	});

	///Comments Config
	Comments.ui.config({
	   template: 'bootstrap' // or ionic, semantic-ui
	});

	//Router Config
	Router.configure({
	  layoutTemplate: 'ApplicationLayout'
	});

	//Routes
	Router.route('/', function () {
	  this.render('navbar', {
	    to:"navbar"
	  });
	  this.render('website', {
	    to:"main"
	  });
	});

	Router.route('/website/:_id', function () {
	  this.render('navbar', {
	    to:"navbar"
	  });
	  this.render('website_page', {
	    to:"main",
	    data:function(){
	      return Websites.findOne({_id:this.params._id});
	    }
	  });
	});

	/////
	// template helpers
	/////

	Template.navbar.helpers({
		search_term:function(){
			if(Session.get("search_term")){

			  return Session.get("search_term");
			}
		}
	});

	// helper function that returns all available websites
	Template.website_list.helpers({
		websites:function(){
			if(Session.get("search_term")){

				return Websites.find({'$or' : [
				  { 'title':{'$regex': Session.get("search_term"), '$options': 'i'} },
				  { 'description':{'$regex':Session.get("search_term"), '$options': 'i'} },
				  ]
				});
			}
			else{
				return Websites.find({},{sort:{upVotes:-1}});
			}
		}
	});

	Template.website_item.helpers({
		localeDate:function(date){
			return date.toLocaleDateString()
		}
	});

	Template.website_page.helpers({
		localeDate:function(date){
			return date.toLocaleDateString()
		}
	});

	/////
	// template events
	/////

	Template.navbar.events({
		"submit .js-search-website-form":function(event){

			// here is an example of how to get the url out of the form:
			var search_term = event.target.search_term.value;

			Session.set("search_term",search_term);

			$("#search_alert").show('slow');

			return false;// stop the form submit from reloading the page

		},
		'click .js-reset-search-website-form':function(){
			Session.set("search_term",undefined);
			$("#search_alert").hide('slow');
			$("form.js-search-website-form").trigger('reset');
		}
	});

	Template.website_item.events({
		"click .js-upvote":function(event){
			// example of how you can access the id for the website in the database
			// (this is the data context for the template)
			var website_id = this._id;
			console.log("Up voting website with id "+website_id);
			// put the code in here to add a vote to a website!

			if (Meteor.user()){
				Websites.update({_id:website_id},
												{$inc: {upVotes: 1 }});
			}

			return false;// prevent the button from reloading the page
		},
		"click .js-downvote":function(event){

			// example of how you can access the id for the website in the database
			// (this is the data context for the template)
			var website_id = this._id;
			console.log("Down voting website with id "+website_id);
			if (Meteor.user()){
				Websites.update({_id:website_id},
												{$inc: {downVotes: 1 }});
			}
			// put the code in here to remove a vote from a website!

			return false;// prevent the button from reloading the page
		},
		'click .js-del-website':function(event){
		   var website_id = this._id;
		   console.log(website_id);
		   // use jquery to hide the website component
		   // then remove it at the end of the animation
		   $("#"+website_id).hide('slow', function(){
		    Websites.remove({"_id":website_id});
		   })
		},
	})

	Template.website_form.events({
		"click .js-toggle-website-form":function(event){
			$("#website_form").toggle('slow');
		},
		"submit .js-save-website-form":function(event){

			// here is an example of how to get the url out of the form:
			var url = event.target.url.value;

			Meteor.call('checkURL', url, function(error, result){
				console.log(result);
				if(result){
					$("#website_form").hide('slow');
					$("#website_form form").trigger('reset');
				}
		  });
			return false;// stop the form submit from reloading the page

		}
	});
}


if (Meteor.isServer) {

	// This code only runs on the server
  Meteor.publish("websites", function () {
    return Websites.find();
  });

	Meteor.methods({
			'checkURL': function(url){

				HTTP.call( 'GET', url, {}, function( error, response ) {
					if ( error ) {
						console.log( error );

					} else {

						var matches = response.content.match(/<title>(.*?)<\/title>/);

						var title = _.unescape(matches[0].replace(/<title>/g, '').replace(/<\/title>/g, ''));

						var description = title;

						matches = response.content.match(/<meta name="description" content="(.*?)">/);

						if(matches != null){
							description = _.unescape(matches[0].replace(/<meta name="description" content="/g, '').replace(/">/g, ''));
						}

						if (Meteor.user()){
							Websites.insert({
								title:title,
								url:url,
								description: description,
								createdOn:new Date(),
								createdBy:Meteor.user()._id
							});
						}

					}

				});
				return true;
			}
	});

	// start up function that creates entries in the Websites databases.
  Meteor.startup(function () {




    // code to run on server at startup
    if (!Websites.findOne()){
    	console.log("No websites yet. Creating starter data.");
    	  Websites.insert({
    		title:"Goldsmiths Computing Department",
    		url:"http://www.gold.ac.uk/computing/",
    		description:"This is where this course was developed.",
    		createdOn:new Date()
    	});
    	 Websites.insert({
    		title:"University of London",
    		url:"http://www.londoninternational.ac.uk/courses/undergraduate/goldsmiths/bsc-creative-computing-bsc-diploma-work-entry-route",
    		description:"University of London International Programme.",
    		createdOn:new Date()
    	});
    	 Websites.insert({
    		title:"Coursera",
    		url:"http://www.coursera.org",
    		description:"Universal access to the world’s best education.",
    		createdOn:new Date()
    	});
    	Websites.insert({
    		title:"Google",
    		url:"http://www.google.com",
    		description:"Popular search engine.",
    		createdOn:new Date()
    	});
    }
  });
}
